<footer class="flex bg-slate-300 p-8 mt-8">
    <div class="container text-center">
        <p>Copyright &copy; <?php echo date('Y'); ?> Dots United GmbH</p>
        <p><a href="https://github.com/dotsunited/falscherIdiot">Sourcecode on GitHub</a></p>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
