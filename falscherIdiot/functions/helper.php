<?php

// Filter except length to x words.
function falscherIdiot_custom_excerpt_length( $length ) {
	return 10;
}

add_filter( 'excerpt_length', 'falscherIdiot_custom_excerpt_length', 999 );

// Custom excerpt more sign
function falscherIdiot_excerpt_more( $more ) {
	return '…';
}

add_filter( 'excerpt_more', 'falscherIdiot_excerpt_more' );

/*
 * Enable svg media upload
 */
function falscherIdiot_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'falscherIdiot_mime_types');

// Bug workaround https://blog.kulturbanause.de/2013/05/svg-dateien-in-die-wordpress-mediathek-hochladen/
function falscherIdiot_ignore_upload_ext($checked, $file, $filename, $mimes){
	
	if(!$checked['type']){
		$wp_filetype = wp_check_filetype( $filename, $mimes );
		$ext = $wp_filetype['ext'];
		$type = $wp_filetype['type'];
		$proper_filename = $filename;
		
		if($type && 0 === strpos($type, 'image/') && $ext !== 'svg'){
			$ext = $type = false;
		}
		
		$checked = compact('ext','type','proper_filename');
	}
	
	return $checked;
}

add_filter('wp_check_filetype_and_ext', 'falscherIdiot_ignore_upload_ext', 10, 4);
