<?php
add_action( 'init', 'registerProjectToolPostType' );
function registerProjectToolPostType() {
	$args = [
		'label'  => esc_html__( 'Project Tools', 'text-domain' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Project Tools', 'falscherIdiot' ),
			'name_admin_bar'     => esc_html__( 'Project Tool', 'falscherIdiot' ),
			'add_new'            => esc_html__( 'Add Project Tool', 'falscherIdiot' ),
			'add_new_item'       => esc_html__( 'Add new Project Tool', 'falscherIdiot' ),
			'new_item'           => esc_html__( 'New Project Tool', 'falscherIdiot' ),
			'edit_item'          => esc_html__( 'Edit Project Tool', 'falscherIdiot' ),
			'view_item'          => esc_html__( 'View Project Tool', 'falscherIdiot' ),
			'update_item'        => esc_html__( 'View Project Tool', 'falscherIdiot' ),
			'all_items'          => esc_html__( 'All Project Tools', 'falscherIdiot' ),
			'search_items'       => esc_html__( 'Search Project Tools', 'falscherIdiot' ),
			'parent_item_colon'  => esc_html__( 'Parent Project Tool', 'falscherIdiot' ),
			'not_found'          => esc_html__( 'No Project Tools found', 'falscherIdiot' ),
			'not_found_in_trash' => esc_html__( 'No Project Tools found in Trash', 'falscherIdiot' ),
			'name'               => esc_html__( 'Project Tools', 'falscherIdiot' ),
			'singular_name'      => esc_html__( 'Project Tool', 'falscherIdiot' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite_no_front'    => false,
		'show_in_menu'        => true,
		'menu_position'       => 25,
		'menu_icon'           => 'dashicons-category',
		'supports' => [
			'title',
			'editor',
			'author',
			'custom-fields',
		],
		
		'rewrite' => true
	];

	register_post_type( 'project-tool', $args );
}