<?php
add_filter( 'rwmb_meta_boxes', 'registerProjectToolsCustomFields' );

function registerProjectToolsCustomFields( $meta_boxes ) {
    $prefix = '';

    $meta_boxes[] = [
        'title'   => esc_html__( 'Project Tools', 'falscherIdiot' ),
        'id'      => 'projectTools',
        'post_types'=> 'project-tool',
        'context' => 'normal',
        'fields'  => [
            [
                'type'        => 'text',
                'name'        => esc_html__( 'Title', 'falscherIdiot' ),
                'id'          => $prefix . 'title',
                'placeholder' => esc_html__( 'Title of Tool', 'falscherIdiot' ),
            ],
            [
                'type'        => 'textarea',
                'name'        => esc_html__( 'Description', 'falscherIdiot' ),
                'id'          => $prefix . 'description',
                'placeholder' => esc_html__( 'Description of tool', 'falscherIdiot' ),
            ],
            [
                'type'        => 'text',
                'name'        => esc_html__( 'Author', 'falscherIdiot' ),
                'id'          => $prefix . 'author',
                'placeholder' => esc_html__( 'Authors Name', 'falscherIdiot' ),
            ],
            [
                'type'        => 'email',
                'name'        => esc_html__( 'email', 'falscherIdiot' ),
                'id'          => $prefix . 'email',
                'placeholder' => esc_html__( 'Email of author', 'falscherIdiot' ),
            ],
            [
                'type'        => 'url',
                'name'        => esc_html__( 'Url', 'falscherIdiot' ),
                'id'          => $prefix . 'url',
                'placeholder' => esc_html__( 'URL to project', 'falscherIdiot' ),
            ],
            [
                'type'        => 'text',
                'name'        => esc_html__( 'Version', 'falscherIdiot' ),
                'id'          => $prefix . 'version',
                'placeholder' => esc_html__( 'Tool version', 'falscherIdiot' ),
            ],
        ],
    ];

    return $meta_boxes;
}