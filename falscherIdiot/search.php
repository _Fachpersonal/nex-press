<?php get_header(); ?>

<div class="container lg:flex mt-12">
    <main class="w-auto lg:w-3/4 mr-8">
        <?php if (have_posts()) :  ?>

            <header class="w-full">
                <h1 class="leading-none m-0 mb-4">
                    <?php _e('Search results for:', 'falscherIdiot'); ?>
                    <strong><?php echo get_search_query(); ?></strong>
                </h1>
            </header>

            <?php while (have_posts()) : the_post(); ?>
                <div class="rounded p-4 mb-4 bg-slate-400">
                    <article id="post-<?php the_ID(); ?>">

                        <h1 class="leading-none">
                            <a class="no-underline hover:underline text-white font-bold" href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'falscherIdiot'), the_title_attribute('echo=0'))); ?>" rel="bookmark">
                                <?php echo the_title(); ?>
                            </a>
                        </h1>

                        <?php if (has_post_thumbnail() && is_singular()) : ?>
                            <div class="mb-4">
                                <?php the_post_thumbnail(); ?>
                            </div>
                        <?php endif; ?>
                    </article>
                </div>
            <?php endwhile; ?>

        <?php else : ?>

            <?php echo falscherIdiot_render('template-parts/components/content/none'); ?>

        <?php endif; ?>

    </main>

    <?php get_sidebar(); ?>
</div>

<?php get_footer();
