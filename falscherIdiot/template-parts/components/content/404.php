<h1 class="leading-tight mb-2"><?php _e('Sorry, that page does not exist.', 'falscherIdiot'); ?></h1>

<p class="font-bold"><?php _e('The page you want to access does not exist, which may have these reasons:', 'falscherIdiot'); ?></p>

<ul>
	<li><?php _e('You made a typo when entering the web address.', 'falscherIdiot'); ?></li>
	<li><?php _e('You clicked on a link that is not correct.', 'falscherIdiot'); ?></li>
</ul>

<p class="font-bold"><?php _e('What can you do? There are several options...', 'falscherIdiot'); ?></p>

<ul>
	<li><a href="javascript:history.back()" title="<?php _e('Back to previous page', 'falscherIdiot'); ?>"><?php _e('Back', 'falscherIdiot'); ?></a> <?php _e('to previous page', 'falscherIdiot'); ?></li>
	<li><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php _e('Visit frontpage', 'falscherIdiot'); ?>"><?php _e('Visit frontpage', 'falscherIdiot'); ?></a></li>
	<li><?php _e('Find the correct page out of the navigation.', 'falscherIdiot'); ?></li>
	<li>
		<?php _e('Try the search function', 'falscherIdiot'); ?>
		<?php get_search_form(); ?>
	</li>
</ul>
